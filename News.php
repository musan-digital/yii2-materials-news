<?php
/**
 * Poject: gakku2
 * User: mitrii
 * Date: 3.11.2016
 * Time: 13:56
 * Original File Name: News.php
 */

namespace lafacoder\modules\materials\types;


class News extends \lafacoder\modules\materials\models\Material
{

    public function init()
    {
        $this->type = 'news';
        parent::init();
    }

    public static function find()
    {
        return new NewsQuery(News::className(), ['type' => 'news']);
    }

    public function beforeSave($insert)
    {
        $this->type = 'news';
        return parent::beforeSave($insert);
    }
}